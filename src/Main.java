import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        char [][] board = new char[5][5];
        initializeBoard(board);

        System.out.println("All set. Get ready to rumble!");

        Scanner scanner = new Scanner(System.in);
        while(true){
            System.out.println("Enter row (1-5): ");
            int row = scanner.nextInt();
            System.out.println("Enter column (1-5): ");
            int column = scanner.nextInt();

            if (isValidInput(row, column)){
                if (board[row-1][column-1] == 'x'){
                    System.out.println("You have won!");
                    break;
                }else if(board[row-1][column-1] == '-'){
                    board[row-1][column-1] = '*';
                }else{
                    System.out.println("You have already shot there. Try again.");
                }
                printBoard(board);
            }else{
                System.out.println("Invalid input. Try again.");
            }
        }
        scanner.close();
    }
    private static void initializeBoard(char[][] board){
        for (char[] row : board){
            Arrays.fill(row, '-');
        }
        int targetRow = (int)(Math.random() * 5);
        int targetColumn = (int)(Math.random() *5);
        board[targetRow][targetColumn] = 'x';
    }
    private static boolean isValidInput(int row, int column){
        return row >= 1 && row <= 5 && column >= 1 && column <=5;
    }
    private static void printBoard(char[][] board){
        System.out.println("  | 1 | 2 | 3 | 4 | 5 |");
        System.out.println("----------------------");
        for (int i = 0; i < 5; i++){
            System.out.print((i + 1) + " | ");
            for (int j = 0; j < 5; j++){
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
        }
    }
}